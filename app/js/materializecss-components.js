(function(){
  'use strict';
})();

$(document).ready(function(){

  // Open Mobile Menu
  $(".mobile-menu-button").sideNav({
    edge: 'right',
    closeOnClick: true
  });

  // Initialize Parallax
  $('.parallax').parallax();

  // Initialize Slider
  $('#slider').slider();


  // Initialize Scroll Spy
  $('.scrollspy').scrollSpy();

});

