(function(){
  'use strict';
})();

$(document).ready(function(){
  var $grid = $('.grid').isotope({
    itemSelector: '.project-item',
    layoutMode: 'fitRows'
  });
  // Bind filter attribute to links
  $('.filters-link-group a').on( 'click', function() {
    var filterValue = $( this ).attr('data-filter');
    $grid.isotope({
      filter: filterValue
    });
  });
  // Add active class to any link that is clicked
  $('.link-group').each(function(){
    var linkItem = $('.link-group a');
    linkItem.on('click', function(){
      linkItem.removeClass('active');
      $(this).addClass('active');
    });
  });
});